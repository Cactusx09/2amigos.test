import Vue from 'vue'

// import 'vue-awesome/icons/trash-alt'
import 'vue-awesome/icons'

import Icon from 'vue-awesome/components/Icon'

Vue.component('v-icon', Icon)
